package com.nedash.lib.data.model

data class AdDataDto(
    val flurryId: String = "",
    val chromeLink: String = "",
    val adDelay: Long = (1000 * 60 * 60 * 3),
    val firstAdDelay: Long = (1000 * 60 * 60 * 24),
    val showOuterAd: Boolean = false,
    val versionWithNoAd: String = ""
)